import React from 'react';

function Book(props){
    let onClick = props.onClick;
    return(
        <div className="answer" 
        onClick={()=>{onClick(props.title);}}
        >
            <h1  style={{
                color: props.selectedChoice.answer === props.title ? props.highlight : ""}} 
            >
                {props.title}

            </h1>
        </div>
    )
}
export default Book;