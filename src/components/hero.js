import React from 'react';
import { Link } from 'react-router-dom';

function Hero() {
    return (
      <div className="row wrapper">
        <div className="col-4 offset-1 header container">
          <h1>Author Quiz</h1>
          <p>Select the book written by the author shown</p>
        </div>
        <div className="button container col-2">
          <button  type="button" class="btn btn-info"><Link to="/add">Add an Author</Link></button>
        </div>
      </div>
    );
}
export default Hero;