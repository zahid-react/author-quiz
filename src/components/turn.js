import React from 'react';
import Book from './book';

function Turn(props){
    let highlight = props.highlight;
    function highlightToBgColor(highlight){
        const mapping = {
            none:'',
            'correct':'green',
            'wrong':'red'
        }
        return mapping[highlight];

    }
    return(
        <div className="row turn">
            <div className="col-4 offset-1">
                <img src={props.author.imageUrl} className="auhtorimage" alt="Auhtor"/>
            </div>
            <div className="col-6">
                {props.books.map((title) => <Book title={title} key={title} highlight={highlightToBgColor(highlight)} selectedChoice={props.selectedChoice} onClick={props.onAnswerSelected}/>)}
            </div>
        </div>
    )
}
export default Turn;
