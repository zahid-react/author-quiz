import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AuthorQuiz from './AuthorQuiz';
import AddAuthorForm from './components/addAuthorForm';
import Authors from './data';
import {shuffle, sample} from 'underscore';
import {BrowserRouter,Route,withRouter} from 'react-router-dom';


function getTurnData(Authors) {
    const allBooks = Authors.reduce(function (p, c, i) {
        return p.concat(c.books);
    }, []);
    const fourRandomBooks = shuffle(allBooks).slice(0,4);
    const answer = sample(fourRandomBooks);

    return {
        books: fourRandomBooks,
        author: Authors.find((author) => 
            author.books.some((title) => 
                title === answer))
    }
}

function resetState(){
    return{
        turnData:getTurnData(Authors),
        highlight:'',
        selectedChoice:''
    }
}

let state = resetState();

function onAnswerSelected(answer){
    const isCorrect = state.turnData.author.books.some((book) => book === answer);
    state.highlight = isCorrect ? 'correct' : 'wrong';
    state.selectedChoice = {answer};
    render();
}


function App(){
    return(
        <AuthorQuiz  
            { ...state }  
            onAnswerSelected={onAnswerSelected} 
            onContinue= { 
                () => {
                    state = resetState();
                    render();
                }
            }
        />
    )
}
const AuthorWrapper = withRouter(({history}) => {
    return(
        <AddAuthorForm onAddAuthor={(author) => {
            Authors.push(author);
            history.push('/');
        }}/>
    )
})

function render(){
    ReactDOM.render(
        <BrowserRouter>
            <React.Fragment>
                <Route exact path="/" component={App}/>
                <Route exact path="/add" component={AuthorWrapper}/>
            </React.Fragment>
        </BrowserRouter>
       ,document.getElementById('root')
    );
}
render();
