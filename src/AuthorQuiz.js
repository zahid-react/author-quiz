import React from 'react';
import './App.css';
import './bootstrap.min.css';
import Hero from './components/hero';
import Continue from './components/continue';
import Footer from './components/footer';
import Turn from './components/turn';


function AuthorQuiz({turnData,highlight,selectedChoice,onAnswerSelected,onContinue}) {
  return (
    <div className="container-fluid">
      <Hero/>
      <Turn {...turnData} highlight={highlight} onAnswerSelected={onAnswerSelected} selectedChoice={selectedChoice}/>
      <Continue show={highlight === 'correct'} onContinue={onContinue}/>
      <Footer/>
    </div>
  );
}

export default AuthorQuiz;
